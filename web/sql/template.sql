/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  USUARIO
 * Created: 11/03/2017
 */
use videoclub;
create table empleados(id int key auto_increment
, nombre varchar(20)
, apellido varchar(20)
, telefono varchar(15)
, email varchar(50)
, cargo varchar(20)
, documento varchar(15)
, fecha_inicio timestamp default current_timestamp
, habilitado boolean default 1
, clave varchar(30)
);
insert into empleados(
nombre
, apellido
, telefono
, email
, cargo
, documento
, clave) values
('steven','Galeano','4333335','s@g.com','admin','1128400998','12345');