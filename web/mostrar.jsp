<%-- 
    Document   : mostrar
    Created on : 8/03/2017, 08:08:53 PM
    Author     : jefferson
--%>

<%@page import="java.util.List"%>
<%@page import="com.clases.EmpleadosDao"%>
<%@page import="com.clases.Empleados"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="head.jsp"></jsp:include>
<jsp:include page="nav.jsp"></jsp:include>

<script>
    function fillmodal(id, nombre, apellido, tel, email, cargo, doc, fecha){
        document.getElementById('id01').style.display = 'block';
        document.getElementById('Id').value = id;
        document.getElementById('Nombre').value = nombre;
        document.getElementById('Apellido').value = apellido;
        document.getElementById('Telefono').value = tel;
        document.getElementById('Email').value = email;
        document.getElementById('Cargo').value = cargo;
        document.getElementById('Documento').value = doc;
        document.getElementById('Fecha').value = fecha;
        

        }
</script>

<% if (session.getAttribute("u") != null) {
        List<Empleados> list = EmpleadosDao.getEmpleados();
        request.setAttribute("list", list);

%>  
<h1 class="w3-blue-grey">Empleados</h1>

<div class="w3-container">
    <table class="w3-table-all w3-card-4 "> 
        <tr><th>Id</th><th>Nombre</th><th>Apellido</th><th>Telefono</th><th>Email</th><th>Cargo</th><th>Documento</th><th>Fecha de ingreso</th>
            <th>Ver</th><th>Editar</th><th>Eliminar</th></tr>  


        <% for (Empleados e : list) {%>    
        <tr><td><%= e.getId()%></td>
            <td><%= e.getNombre()%></td>
            <td><%= e.getApellido()%></td>  
            <td><%= e.getTelefono()%></td>
            <td><%= e.getEmail()%></td>
            <td><%= e.getCargo()%></td>  
            <td><%= e.getDocumento()%></td>
            <td><%= e.getFInicio()%></td>
            <td><a href="Ver?id=<%= e.getId()%>" ><i class="w3-xlarge fa fa-eye" ></i></a></td>
            <td><a href="#id01" onclick="fillmodal(<%= e.getId()%>, '<%= e.getNombre()%>', '<%= e.getApellido()%>', '<%= e.getTelefono()%>', '<%= e.getEmail()%>', '<%= e.getCargo()%>', '<%= e.getDocumento()%>', '<%= e.getFInicio()%>')"><i class="w3-xlarge fa fa-pencil-square-o" ></i></a></td>
            <td><a href="Eliminar?id=<%= e.getId()%>"><i class="w3-xlarge w3- fa fa-trash" ></i></a></td>
        </tr> 
        <% } %>

    </table>
</div>
<% } else {
%>Usted no tiene permiso<%}%>

<jsp:include page="ventana.jsp"></jsp:include>



<jsp:include page="footer.html"></jsp:include>

