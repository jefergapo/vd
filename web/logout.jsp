<%-- 
    Document   : logout
    Created on : 10/03/2017, 08:50:54 PM
    Author     : jefferson
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    
    String u = (String)session.getAttribute("u");
    
    session = request.getSession(false);
    if(session!=null){
    session.invalidate();
    }
%>

<jsp:include page="head.jsp"></jsp:include>
    
    <jsp:include page="nav.jsp"></jsp:include>
    
    Hasta pronto <%= u%>
    
    <jsp:include page="footer.html"></jsp:include>