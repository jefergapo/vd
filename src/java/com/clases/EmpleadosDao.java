/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clases;

/**
 *
 * @author jefferson
 */
import java.util.*;
import java.sql.*;

public class EmpleadosDao {

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/videoclub", "jefferson", "12345s");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        return con;
    }

    public static int save(Empleados e) {
        int status = 0;
        try {
            Connection con = EmpleadosDao.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into empleados(nombre, apellido, telefono, email, cargo, documento, clave) VALUES (?,?,?,?,?,?,?)");
             
            ps.setString(1, e.getNombre());
            ps.setString(2, e.getApellido());
            ps.setString(3, e.getTelefono());
            ps.setString(4, e.getEmail());
            ps.setString(5, e.getCargo());
            ps.setString(6, e.getDocumento());
            ps.setString(7, e.getClave());

            status = ps.executeUpdate();

            con.close();
        } catch (SQLException ex) {
        }

        return status;
    }

    public static List<Empleados> getEmpleados() {
        List<Empleados> list = new ArrayList<>();

        try {
            try (Connection con = EmpleadosDao.getConnection()) {
                PreparedStatement ps = con.prepareStatement("select * from empleados");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Empleados e = new Empleados();
                    e.setId(rs.getInt(1));
                    e.setNombre(rs.getString(2));
                    e.setApellido(rs.getString(3));
                    e.setTelefono(rs.getString(4));
                    e.setEmail(rs.getString(5));
                    e.setCargo(rs.getString(6));
                    e.setDocumento(rs.getString(7));
                    e.setFInicio(rs.getDate(8).toString());
                    
                    
                    list.add(e);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
    
    
    public static Empleados consulta(String email) {
        
         Empleados e = new Empleados();

        try {
             try (Connection con = EmpleadosDao.getConnection()) {
                 PreparedStatement ps = con.prepareStatement("select * from empleados where email=?");
                 ps.setString(1, email);
                 ResultSet rs = ps.executeQuery();
                 while (rs.next()) {
                     
                     e.setId(rs.getInt(1));
                     e.setNombre(rs.getString(2));
                     e.setApellido(rs.getString(3));
                     e.setTelefono(rs.getString(4));
                     e.setEmail(rs.getString(5));
                     e.setCargo(rs.getString(6));
                     e.setDocumento(rs.getString(7));
                    e.setFInicio(rs.getDate(8).toString());
                     e.setClave(rs.getString(10));
                 }}
        } catch (SQLException a) {
            a.printStackTrace();
        }

        return e;
    }
    
    public static int delete(int id) {
        
         Empleados e = new Empleados();
         int status = 0;
        try {
             try (Connection con = EmpleadosDao.getConnection()) {
                 PreparedStatement ps = con.prepareStatement("delete from empleados where id=?");
                 ps.setInt(1,id);
                 status = ps.executeUpdate();
                 }
        } catch (SQLException a) {
            a.printStackTrace();
        }

        return status;
    }
    
    public static int update(Empleados e) {
        int status = 0;
        try {
            Connection con = EmpleadosDao.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE empleados SET telefono=?, email=?, cargo=? WHERE id=? ");
             
            ps.setString(1, e.getTelefono());
            ps.setString(2, e.getEmail());
            ps.setString(3, e.getCargo());
            ps.setInt(4, e.getId());
            

            status = ps.executeUpdate();

            con.close();
        } catch (SQLException ex) {
        }

        return status;
    }

}
