/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlets;

import com.clases.Empleados;
import com.clases.EmpleadosDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jefferson
 */
public class Guardar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");  
        try (PrintWriter out = response.getWriter()) {
            String nombre=request.getParameter("nombre");
            String apellido=request.getParameter("apellido");
            String telefono=request.getParameter("telefono");
            String email=request.getParameter("email");
            String cargo=request.getParameter("cargo");
            String documento=request.getParameter("documento");
            String clave=request.getParameter("clave");
            
            Empleados e=new Empleados();
            
            e.setNombre(nombre);
            e.setApellido(apellido);
            e.setTelefono(telefono);
            e.setEmail(email);
            e.setCargo(cargo);
            e.setDocumento(documento);
            e.setClave(clave);
            
            
            int status=EmpleadosDao.save(e);
            if(status>0){
                out.print("<p>Registro guardado!</p>");
                request.getRequestDispatcher("registrar.jsp").include(request, response);
            }else{
                out.println("Error");
            }
        }  
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
