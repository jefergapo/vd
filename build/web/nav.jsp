<div class="w3-bar w3-border w3-light-grey">
    <a class="w3-bar-item w3-button" href="index.jsp">Home</a>
    
    <% if (session.getAttribute("u") != null) { %>
        <a class="w3-bar-item w3-button" href="mostrar.jsp">Mostrar</a>
        <a class="w3-bar-item w3-button" href="registrar.jsp">Registrar</a>
    <% }%>
    
    <% if (session.getAttribute("u") != null) { %>
        <a class="w3-bar-item w3-button w3-green w3-right" href="logout.jsp">cerrar session</a>
    <% }%>
    
    <a class="w3-bar-item w3-button w3-green w3-right" href="index.jsp">
        
    <% if (session.getAttribute("u") == null) { %>
        Iniciar session</a> 
    <% } else { %>  
        <%= session.getAttribute("u") %></a> <% }%>

</div>  